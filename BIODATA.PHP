<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>latihan 1</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
    
</head>
<body class="biru">
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="biodata.html">biodata</a> 
              </li>
              <li class="nav-item">
            </li>
            <li class="nav-item">
              <a class="nav-link" href="berita.html">Berita</a> 
            </li>
            <li class="nav-item">
              <a class="nav-link" href="galeri.html">Galeri</a> 
            </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Dropdown
                </a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#">Action</a></li>
                  <li><a class="dropdown-item" href="#">Another action</a></li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                </ul>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" aria-disabled="true">Disabled</a>
              </li>
            </ul>
            <form class="d-flex" role="search">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <img src="image.png" alt="anime" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card body bg-info">
                    <table class="table table-hover table-warning">
                        <thead> 
                            <h1 class="text-center">biodata diri</h1>
                        </thead>
                        <tbody class="rtl">
                            <tr>
                                <td>NPM</td>
                                <td>:</td>
                                <td>021220037</td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td>:</td>
                                <td>Nadia puspita</td>
                            </tr>
                            <tr>
                                <td>jenis kelamin</td>
                                <td>:</td>
                                <td>perempuan</td>
                            </tr>
                            <tr>
                                <td>tempat tanggal lahir</td> 
                                <td>:</td>
                                <td>banyuasin 02 desember 2002</td>   
                            </tr>
                            <tr>
                                <td>alamat</td>
                                <td>:</td>
                                <td>ds tirtaharja</td>
                            </tr>
                            <tr>
                                <td>No Hp</td>
                                <td>:</td>
                                <td>088287970094</td>
                            </tr>
                            <tr>
                                <td>hobi</td>
                                <td>:</td>
                                <td>
                                    <ul>
                                        <li>traveling</li>
                                        <li>olahraga</li>
                                        <li>membaca</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>sekolah</td>
                                <td>:</td>
                                <td>
                                    
                                    <ol>
                                        <li>SDN 20 muara sugihan</li>
                                        <li>ponpes daarul abroor</li>
                                    </ol>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>   
    
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum ipsa cumque id labore illum, reiciendis quae sequi veritatis exercitationem ipsam perspiciatis architecto adipisci natus corrupti molestias sit, ab, enim sunt!
</div>  
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script> 
</body>
</html>